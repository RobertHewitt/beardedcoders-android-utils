package com.beardedcoders.android.utils.math;

import junit.framework.TestCase;

/**
 * Created with IntelliJ IDEA.
 * User: Rob_2
 * Date: 07/05/12
 * Time: 18:24
 * To change this template use File | Settings | File Templates.
 */
public class UtilMathTest
		extends TestCase
{
	public void testDivideInColumns() throws
			Exception
	{

		int[] output = UtilMath.splitRangeByParts(2, 1);
		assertEquals(output[ 0 ], 0);
		assertEquals(output[ 1 ], 2);
		assertEquals(output.length, 2);


		output = UtilMath.splitRangeByParts(10, 10);
		assertEquals(output.length, 11);
		for ( int i = 0; i < output.length; i++ )
		{
			assertEquals(output[ i ], i);
		}
	}
}
