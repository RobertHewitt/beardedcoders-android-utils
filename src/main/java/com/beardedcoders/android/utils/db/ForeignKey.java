package com.beardedcoders.android.utils.db;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created with IntelliJ IDEA.
 * User: Rob_2
 * Date: 04/08/12
 * Time: 16:36
 * To change this template use File | Settings | File Templates.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface ForeignKey
{
	Class<? extends BCTable> table();
	String column();
}
