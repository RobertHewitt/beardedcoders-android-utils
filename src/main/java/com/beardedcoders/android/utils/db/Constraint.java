package com.beardedcoders.android.utils.db;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created with IntelliJ IDEA.
 * User: Rob_2
 * Date: 04/08/12
 * Time: 16:28
 * To change this template use File | Settings | File Templates.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Constraint
{
	boolean primaryKey() default false;
	boolean autoIncrement() default false;

	/**
	 * if you specify that the you do not allow null, you have to specify a default value
	 * this is due to all sqliteColumns having a default value of NULL if not specified.
	 * @return
	 */
	boolean allowNull() default false;
	boolean unique() default false;
	String defaultValue() default "NULL";
}
