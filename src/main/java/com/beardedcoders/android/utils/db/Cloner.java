package com.beardedcoders.android.utils.db;

import java.util.List;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Provides functionality to clone from one Sqlite to another.
 * TODO: Actually make this class work as the Sqlite database does not contain 
 * @author bilbovb
 *
 */
public final class Cloner
{
	@SuppressWarnings("unused")
	private static final String SQL_LIST_TABLES = "SELECT name FROM SQLITE_MASTER WHERE type = 'table'";
	
	/**
	 * Clone all the tables from one Sqlite database to another.
	 * @param source the source sqlite database already open in readable mode at least.
	 * @param destination
	 */
	public void cloneToSd(SQLiteDatabase source, String sqlCreateDb, String destination)
	{
		if (source == null) throw new NullPointerException("Source database cannot be null");
		if (sqlCreateDb == null) throw new NullPointerException("CreateDb SQL cannot be null");
		if (destination == null) throw new NullPointerException("destination to create the table cannot be null");
		
		
		//List<String> tables = new ArrayList<String>();
		Cursor cursor = source.rawQuery(sqlCreateDb, null);
		if (cursor != null)
		{
			
		}
	}
	
	@SuppressWarnings("unused")
	private List<String> getTableNames(SQLiteDatabase source)
	{
		return null;
	}
}
