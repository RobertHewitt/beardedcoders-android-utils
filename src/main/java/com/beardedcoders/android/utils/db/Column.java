package com.beardedcoders.android.utils.db;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created with IntelliJ IDEA.
 * User: Rob_2
 * Date: 04/08/12
 * Time: 15:27
 * To change this template use File | Settings | File Templates.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Column
{
	/**
	 * this is NOT intended to be used to force an update of a column. This will cause a breaking change and crash
	 * SQLite3 does NOT support modifying an existing column.
	 * You are stuck with the columns you create, therefore make sure you create them correctly
	 * @return
	 */
	int dbVersionIntroducedIn();

	/**
	 * convenience for the creation boilerplate to know what to insert for the SQL creation
	 * @return
	 */
	DataType dataType();

	public static enum DataType
	{
		INTEGER ("INTEGER")
		, TEXT ("TEXT")
		, BLOB ("BLOB")
		, NUMERIC ("NUMERIC")
		, BOOLEAN ("BOOLEAN")
		, TINYINT ("TINYINT")
		, DOUBLE ("DOUBLE")
		, FLOAT ("FLOAT")
		, DATETIME ("DATETIME")
		;

		public String sqlType;

		DataType( String sqlType )
		{
			this.sqlType = sqlType;
		}
	}
}