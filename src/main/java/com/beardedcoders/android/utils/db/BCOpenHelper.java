package com.beardedcoders.android.utils.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Date: 04/08/12
 * Time: 15:43
 *
 * @author Robert Hewitt
 * @since 1.1.0
 *        This class is designed to be a helper way of handling table creation using annotation.
 *        Conceptually you only have to subclass BCTable and add in the fields you want declaring
 *        them with {@link Column} annotation.
 *        This class will handle constructing your SQL for you to create the table.
 *        <p/>
 *        Upgrade is also taken care of so long as you mark the dbVersionIntroducedIn on the {@link Column}
 *        and {@link Table} annotations
 *        <p/>
 *        NB SQLite does NOT allow the altering of columns in a table after creation, this also stands for constraints etc.
 */
public abstract class BCOpenHelper
  extends SQLiteOpenHelper
{
	private static final String TAG = "BCOpenHelper";

	public BCOpenHelper( Context context, String name, SQLiteDatabase.CursorFactory factory, int version )
	{
		super(context, name, factory, version);
	}

	@Override
	public void onCreate( SQLiteDatabase sqLiteDatabase )
	{
		Log.d(TAG, "BCOpenHelper.onCreate");
		// sweet! - we're creating a Db for the first time - everyone likes databases
		final List<Class<? extends BCTable>> tablesUsed = getTablesUsed();
		StringBuilder stringBuilder = new StringBuilder();
		for ( Class<? extends BCTable> aClass : tablesUsed )
		{
			sqLiteDatabase.execSQL(calcSQLCreationForTable(aClass, stringBuilder));
		}
	}

	@Override
	public void onUpgrade( SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion )
	{
		Log.d(TAG, "BCOpenHelper.onUpgrade");
		Log.d(TAG, "oldVersion = " + oldVersion);
		Log.d(TAG, "newVersion = " + newVersion);
		final List<Class<? extends BCTable>> tablesUsed = getTablesUsed();

		if ( onUpgradeDropTables() )
		{
			dropTables(sqLiteDatabase);
		}

		// for each table, decide if we want to create or update
		StringBuilder sb = new StringBuilder(); // used for creation strings
		for ( Class<? extends BCTable> table : tablesUsed )
		{
			int introducedIn = getTableIntroducedIn(table);
			Log.d(TAG, "table.getSimpleName() = " + table.getSimpleName());
			Log.d(TAG, "introducedIn = " + introducedIn);
			if ( onUpgradeDropTables() || oldVersion < introducedIn )
			{
				Log.d(TAG, "compiling the create table statement");
				preTableCreation(sqLiteDatabase, table);
				sqLiteDatabase.execSQL(calcSQLCreationForTable(table, sb));
				postTableCreation(sqLiteDatabase, table);
			}
			else
			{
				Log.d(TAG, "checking for any new columns added to table");
				// we need to upgrade this table - possibly, need to check each column
				upgradeTableIfRequired(sqLiteDatabase, table, oldVersion, sb);
			}
		}
	}

	private void dropTables( SQLiteDatabase db )
	{
		Log.d(TAG, "dropping all tables");
		final Cursor tables = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'" +
		  " AND name not like 'android_metadata' AND name NOT LIKE 'sqlite_sequence';", null);
		tables.moveToFirst();
		while ( !tables.isAfterLast() )
		{
			final String tableName = tables.getString(0);
			final String sql = "DROP TABLE " + tableName;
			Log.d(TAG, "sql = " + sql);
			db.execSQL(sql);
			tables.moveToNext();
		}
	}

	/**
	 * allows the subclass to determine if we should wipe tables on upgrade.
	 * Default is false.
	 * @return
	 */
	protected boolean onUpgradeDropTables() {
		return false;
	}

	/**
	 * this will handle upgrading this table to the new structure of this table if any changes have occurred
	 * on the columns of this table based on the existingDbVersion (the one we currently house in this context)
	 *
	 * @param sqLiteDatabase
	 * @param table
	 * @param existingDbVersion
	 * @param stringBuilder
	 */
	private void upgradeTableIfRequired( SQLiteDatabase sqLiteDatabase, Class<? extends BCTable> table, int existingDbVersion, StringBuilder stringBuilder )
	{
		wipeContents(stringBuilder);
		// check each field and determine if it is new and requires a modification to the table
		final ArrayList<Field> sqliteFields = getSqliteFields(table);
		for ( Field field : sqliteFields )
		{
			final int introducedIn = getFieldIntroducedIn(field);
			if ( introducedIn > existingDbVersion )
			{
				preTableUpgradeColumnInsert(table, field, sqLiteDatabase);
				Log.d(TAG, "need to insert column into table detected! table.getSimpleName() = " + table.getSimpleName() + "field = " + field);
				stringBuilder.append("\nALTER TABLE ");
				stringBuilder.append(table.getAnnotation(Table.class).name());
				stringBuilder.append(" ADD COLUMN \n");
				extractColumnDef(table, field, true, stringBuilder);
				String sql = stringBuilder.toString();
				Log.d(TAG, "adding new column -- sql = " + sql);
				sqLiteDatabase.execSQL(sql);
				postTableUpdateColumnInsert(table, field, sqLiteDatabase);
			}
		}
	}

	private int getFieldIntroducedIn( Field field )
	{
		return field.getAnnotation(Column.class).dbVersionIntroducedIn();
	}

	private int getTableIntroducedIn( Class<? extends BCTable> table )
	{
		return table.getAnnotation(Table.class).dbVersionIntroducedIn();
	}

	private String calcSQLCreationForTable( Class<? extends BCTable> aClass, StringBuilder stringBuilder )
	{
		wipeContents(stringBuilder);
		final Table annotationTable = performSanityChecks(aClass);
		Log.d(TAG, "annotationTable.name() = " + annotationTable.name());
		stringBuilder.append("CREATE TABLE ").append(annotationTable.name()).append("( \n");
		final ArrayList<Field> sqliteFields = getSqliteFields(aClass);

		boolean primaryKeyDeclared = false; // used to check we only have one primary key per table
		List<FKey> foreignKeys = new ArrayList<FKey>(); // used to parse the foreign keys after column creation
		for ( int i = 0; i < sqliteFields.size(); i++ )
		{
			Field field = sqliteFields.get(i);
			storeForeignKeyForLater(foreignKeys, field);
			primaryKeyDeclared = extractColumnDef(aClass, field, primaryKeyDeclared, stringBuilder);
			if ( i < sqliteFields.size() - 1 )
			{
				stringBuilder.append(" , \n"); // if we're not at the end of our columns
			}
		}

		if ( !primaryKeyDeclared )
		{
			// if we have no declared primary keys, this falls over - shouldn't happen as BCTable has one
			throw new IllegalStateException("no primary key is defined for table: " + annotationTable.name());
		}

		// process any foreign key constraints we might have had.
		if ( foreignKeys.size() >= 1 )
		{
			for ( FKey key : foreignKeys )
			{
				stringBuilder.append(", \n");
				stringBuilder.append(" FOREIGN KEY(").append(key.field.getName()).append(") REFERENCES ");
				stringBuilder.append(key.key.table().getAnnotation(Table.class).name());
				stringBuilder.append("(").append(key.key.column()).append(") ");
			}
		}

		stringBuilder.append(")");
		String sql = stringBuilder.toString();
		Log.d(TAG, "compiled sql for tbl-" + annotationTable.name() + " = \n" + sql);
		return sql;
	}

	private void storeForeignKeyForLater( List<FKey> keys, Field field )
	{
		final ForeignKey foreignKey = field.getAnnotation(ForeignKey.class);
		if ( foreignKey != null )
		{
			keys.add(new FKey(foreignKey, field));
		}
	}

	private Table performSanityChecks( Class<? extends BCTable> aClass )
	{
		final Table annotationTable = aClass.getAnnotation(Table.class);
		if ( annotationTable == null || TextUtils.isEmpty(annotationTable.name()) )
		{
			throw new IllegalArgumentException("the following class did not fill in the annotation Table: " + aClass.getSimpleName());
		}
		return annotationTable;
	}

	private void wipeContents( StringBuilder stringBuilder )
	{
		if ( stringBuilder.length() != 0 )
		{
			stringBuilder.delete(0, stringBuilder.length());
		}
	}

	/**
	 * this inserts into the string builder the columnDef for this field.
	 * the class is used for the exception to throw if / when two primary keys are detected
	 *
	 * @param aClass
	 * @param field
	 * @param primaryKeyDeclared if the current class already has a primary key declared
	 * @param stringBuilder
	 * @return
	 */
	private boolean extractColumnDef( Class<? extends BCTable> aClass, Field field, boolean primaryKeyDeclared, StringBuilder stringBuilder )
	{
		Log.d(TAG, "field.getName() = " + field.getName());
		final Column column = field.getAnnotation(Column.class);
		final Constraint constraint = field.getAnnotation(Constraint.class);
		extractColumnBasicDefinition(stringBuilder, field, column);
		primaryKeyDeclared = extractConstraintDefinition(aClass, stringBuilder, primaryKeyDeclared, constraint);
		return primaryKeyDeclared;
	}

	private ArrayList<Field> getSqliteFields( Class<? extends BCTable> aClass )
	{
		final ArrayList<Field> sqliteFields = new ArrayList<Field>();
		getFieldsFromClass(aClass.getSuperclass(), sqliteFields); // get fields from super (beardedTable)
		getFieldsFromClass(aClass, sqliteFields); // get fields from class (whatever we are)

		if ( sqliteFields.size() <= 1 )
		{
			Log.d(TAG, "sqliteFields.size() = " + sqliteFields.size());
			throw new IllegalStateException("the following class has no declared fields - pointless table, fix: " + aClass.getSimpleName());
		}
		return sqliteFields;
	}

	private void getFieldsFromClass( Class aClass, ArrayList<Field> sqliteFields )
	{
		final Field[] classLevelFields = aClass.getDeclaredFields();
		for ( Field field : classLevelFields )
		{
			if ( field.getAnnotation(Column.class) != null )
			{
				// hit an actual field we care about
				sqliteFields.add(field);
			}
		}
	}

	private static class FKey
	{
		final ForeignKey key;
		final Field field;

		FKey( ForeignKey key, Field field )
		{
			this.key = key;
			this.field = field;
		}
	}

	private boolean extractConstraintDefinition( Class<? extends BCTable> aClass, StringBuilder stringBuilder, boolean primaryKeyDeclared, Constraint constraint )
	{
		if ( constraint != null )
		{
			if ( constraint.primaryKey() )
			{
				if ( primaryKeyDeclared )
				{
					throw new IllegalStateException("two primary keys are declared on the following class: " + aClass.getSimpleName());
				}
				primaryKeyDeclared = true;
				stringBuilder.append(" PRIMARY KEY ");

				if ( constraint.autoIncrement() )
				{
					stringBuilder.append(" AUTOINCREMENT ");
				}
			}
			else
			{

				if ( !constraint.allowNull() && ( TextUtils.isEmpty(constraint.defaultValue()) || constraint.defaultValue().equalsIgnoreCase("NULL") ) )
				{
					throw new IllegalStateException("you have specified a column to not allow null but provided no default");
				}

				if ( constraint.unique() )
				{
					stringBuilder.append(" UNIQUE ");
				}

				if ( !constraint.allowNull() )
				{
					stringBuilder.append(" NOT NULL ");
				}

				if ( constraint.defaultValue().length() >= 1 )
				{
					stringBuilder.append(" DEFAULT ");
					stringBuilder.append(constraint.defaultValue());
				}
			}
		}
		return primaryKeyDeclared;
	}

	private void extractColumnBasicDefinition( StringBuilder stringBuilder, Field declaredField, Column column )
	{
		stringBuilder.append(" ");
		stringBuilder.append(declaredField.getName());
		stringBuilder.append(" ");
		stringBuilder.append(column.dataType().sqlType);
		stringBuilder.append(" ");
	}

	/**
	 * this allows us to pass down an array of classes we want to use as tables
	 * internally the boilerplate for creating these tables will be performed for us
	 * if the onCreate() method has to be called
	 * <p/>
	 * it is important that the order of the classes passed through are the creation order
	 * This is relevant if you have foreign keys
	 *
	 * @return
	 */
	protected abstract List<Class<? extends BCTable>> getTablesUsed();

	/**
	 * hook to let you know we are about to create this table as it didn't previously exist.
	 *
	 * @param sqLiteDatabase
	 * @param table
	 */
	protected void preTableCreation( SQLiteDatabase sqLiteDatabase, Class<? extends BCTable> table )
	{
	}

	/**
	 * hook to let you know that we just created this table due to it being added to your schema
	 *
	 * @param sqLiteDatabase
	 * @param table
	 */
	protected void postTableCreation( SQLiteDatabase sqLiteDatabase, Class<? extends BCTable> table )
	{
	}

	/**
	 * hook to let you know we have inserted this column into your table.
	 *
	 * @param table
	 * @param field
	 * @param sqLiteDatabase
	 */
	protected void postTableUpdateColumnInsert( Class<? extends BCTable> table, Field field, SQLiteDatabase sqLiteDatabase )
	{
	}

	/**
	 * hook to let you know we're about to insert a new column to the db due to your table def changing.
	 *
	 * @param table
	 * @param field
	 * @param sqLiteDatabase
	 */
	protected void preTableUpgradeColumnInsert( Class<? extends BCTable> table, Field field, SQLiteDatabase sqLiteDatabase )
	{
	}

}
