package com.beardedcoders.android.utils.db;

/**
 * Created with IntelliJ IDEA.
 * User: Rob_2
 * Date: 04/08/12
 * Time: 15:38
 * </p>
 * This is created as a basic representation of a table with default columns for the
 * primary keys used in sqlite which is _id
 * <p/>
 * The annotations are used to compile the creation sql to save boilerplate
 */
public abstract class BCTable
{
	@Column(dbVersionIntroducedIn = 1, dataType = Column.DataType.INTEGER)
	@Constraint(primaryKey = true, autoIncrement = true)
	int _id;
}
