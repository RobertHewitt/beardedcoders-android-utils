package com.beardedcoders.android.utils.db;

import android.content.*;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Date: 05/08/12
 * Time: 15:33
 *
 * @author Robert Hewitt
 * @since 1.1.0
 *        <p/>
 *        The intention of this class is to provide a seemless way of binding datatable classes to a content provider.
 *        The process should take classes that extend {@link BCTable} and figure out from the annotations
 *        1. what we want to share through the data provder
 *        2. provide boiler plate so we don't need to do much if anything.
 */
public abstract class BCContentProvider
  extends ContentProvider
{
	private static final String TAG = "BCContentProvider";
	private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
	private static final Map<String, List<MatchedWrapper>> mappedUriPaths = new HashMap<String, List<MatchedWrapper>>();
	private static final Object lock = new Object();
	private static Integer mapValueIndex = 0;
	private final String authority;
	private BCOpenHelper openHelper;

	private synchronized int getNextMapIndex()
	{
		mapValueIndex++;
		return mapValueIndex;
	}

	private static enum MatchType {
		WHOLE_TABLE, SINGLE_ROW
	}

	private static final class MatchedWrapper
	{
		final String tableName;
		final MatchType type;
		final int index;

		private MatchedWrapper( String tableName, MatchType type, int index )
		{
			this.tableName = tableName;
			this.type = type;
			this.index = index;
		}

		public String getPath()
		{
			switch ( type )
			{
				case WHOLE_TABLE:
					return tableName;
				case SINGLE_ROW:
				default:
					return tableName + "/#";
			}
		}

		@Override
		public String toString()
		{
			return "MatchedWrapper{" +
			  "tableName=" + tableName +
			  ", type=" + type +
			  ", index=" + index +
			  '}';
		}
	}
	public BCContentProvider( String authority )
	{
		this.authority = authority;
	}

	private void mapOpenHelperToUriMatcher()
	{
		final List<Class<? extends BCTable>> tablesUsed = openHelper.getTablesUsed();
		if ( tablesUsed == null )
		{
			return;
		}

		// we lock to ensure that we only initialise a class with it's tables once.
		synchronized ( lock )
		{
			if ( mappedUriPaths.containsKey(this.getClass().getSimpleName()) )
			{
				return;  // quick exit
			}

			for ( Class<? extends BCTable> table : tablesUsed )
			{
				storeMappedUri(new MatchedWrapper(table.getAnnotation(Table.class).name(), MatchType.WHOLE_TABLE, getNextMapIndex()));
				storeMappedUri(new MatchedWrapper(table.getAnnotation(Table.class).name(), MatchType.SINGLE_ROW, getNextMapIndex()));
			}
		}
	}

	private void storeMappedUri( MatchedWrapper matchedWrapper )
	{
		Log.d(TAG, "BCContentProvider.storeMappedUri");
		Log.d(TAG, "matchedWrapper = " + matchedWrapper);
		final String simpleName = this.getClass().getSimpleName();
		List<MatchedWrapper> matchedWrappers = mappedUriPaths.get(simpleName);
		if ( matchedWrappers == null )
		{
			matchedWrappers = new ArrayList<MatchedWrapper>();
			mappedUriPaths.put(simpleName, matchedWrappers);
		}
		matchedWrappers.add(matchedWrapper);
		uriMatcher.addURI(authority, matchedWrapper.getPath(), matchedWrapper.index);
	}

	@Override
	public boolean onCreate()
	{
		openHelper = getOpenHelper(getContext());
		mapOpenHelperToUriMatcher();
		return true;
	}

	public abstract BCOpenHelper getOpenHelper( Context context );

	@Override
	public Cursor query( Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
	{
		Log.d(TAG, "BCContentProvider.query");
		final MatchedWrapper queryTable = getMatchedWrapper(uri);
		if ( queryTable == null )
		{
			return null;
		}
		SQLiteQueryBuilder sqLiteQueryBuilder = new SQLiteQueryBuilder();
		sqLiteQueryBuilder.setTables(( queryTable.tableName ));

		if ( queryTable.type == MatchType.SINGLE_ROW )
		{
			sqLiteQueryBuilder.appendWhere("_id = " + ContentUris.parseId(uri));
		}
		final Cursor query = sqLiteQueryBuilder.query(openHelper.getReadableDatabase(), projection, selection, selectionArgs, null, null, sortOrder);
		query.setNotificationUri(getContext().getContentResolver(), uri);
		return query;
	}

	@Override
	public Uri insert( Uri uri, ContentValues contentValues )
	{
		final MatchedWrapper matchedWrapper = getMatchedWrapper(uri);
		if ( matchedWrapper == null )
		{
			return null;
		} else if ( matchedWrapper.type != MatchType.WHOLE_TABLE )
		{
			Log.d(TAG, "the matched URI did not match a table, must match to a table to perform an insert");
			Log.d(TAG, "matchedWrapper.type = " + matchedWrapper.type);
			return null;
		}
		// process the request, inserting the row and returning a URI that will return the inserted row.
		final SQLiteDatabase writableDatabase = openHelper.getWritableDatabase();
		final long insertIndex = writableDatabase.insertOrThrow(matchedWrapper.tableName, null, contentValues);
		return ContentUris.withAppendedId(uri, insertIndex);
	}

	@Override
	public int delete( Uri uri, String selection, String[] selectionArgs)
	{
		final MatchedWrapper matchedWrapper = getMatchedWrapper(uri);
		if ( matchedWrapper == null )
		{
			return 0;
		} else if ( matchedWrapper.type != MatchType.SINGLE_ROW )
		{
			Log.d(TAG, "we only accept deletes of a single row");
			return 0;
		}
		// process the request, inserting the row and returning a URI that will return the inserted row.
		final SQLiteDatabase writableDatabase = openHelper.getWritableDatabase();
		return writableDatabase.delete(matchedWrapper.tableName, "_id = " + ContentUris.parseId(uri), null);
	}

	@Override
	public int update( Uri uri, ContentValues contentValues, String selection, String[] selectionArgs)
	{
		Log.d(TAG, "BCContentProvider.update");
		throw new UnsupportedOperationException("not currently supported");
	}

	@Override
	public String getType( Uri uri )
	{
		Log.d(TAG, "BCContentProvider.getType");
		throw new UnsupportedOperationException("not currently supported");
	}

	private MatchedWrapper getMatchedWrapper( Uri uri )
	{
		final int matchedUriIndex = uriMatcher.match(uri);

		if ( matchedUriIndex == UriMatcher.NO_MATCH )
		{
			Log.d(TAG, "uri passed through did not match any mapped uris");
			return null;
		}

		final List<MatchedWrapper> matchedWrappers = mappedUriPaths.get(this.getClass().getSimpleName());
		if ( matchedWrappers == null )
		{
			Log.d(TAG, "nothing to see here, no tables matched against this class :(");
			return null; // nothing mapped against this class :(
		}

		for ( MatchedWrapper matchedWrapper : matchedWrappers )
		{
			if ( matchedWrapper.index == matchedUriIndex )
			{
				return matchedWrapper;
			}
		}

		throw new IllegalStateException("weird - we had a matched URI index, but failed to get it out of the mapped wrappers?" +
		  " cry and talk to your deity");
	}
}