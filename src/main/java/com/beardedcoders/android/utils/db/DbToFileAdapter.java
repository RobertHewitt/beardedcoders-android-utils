package com.beardedcoders.android.utils.db;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

/**
 * Backup a Sqlite table to a file.  Mainly because without rooting a device we
 * cannot access it on a real phone.
 * @author bilbovb
 *
 */
public class DbToFileAdapter
{
	private SQLiteDatabase database;
	private String delimiter = "$";
		//�";
	private String newline = "\n";
	private String nullString = "[null]";
	private StringBuilder sb;
	
	/**
	 * Create a BackupToFile object providing the SQLiteDatabase and Context to support it.
	 * @param database SQLiteDatabase to read from.
	 * @param context the context to use.
	 */
	public DbToFileAdapter(SQLiteDatabase database)
	{
		this.database = database;
	}
	
	/**
	 * Write the entire contents of a table to a file.
	 * @param table the name of the table.
	 * @param file the file to write the table to.  Do not provide a path as this is automatically selected.
	 * @throws IOException if an error occurs when opening the file or writing to it.
	 */
	public void writeToFile(String table, String file) throws IOException
	{
		if (table == null) throw new NullPointerException("table to export cannot be null");
		if (file == null) throw new NullPointerException("file to export to cannot be null");
		
		// TODO Sql injection not safe do we care
		// TODO: Trap the exceptions and deal with them perhaps through some unchecked funtimes
		Cursor results = database.rawQuery("SELECT * FROM " + table , null);
		if (results != null)
		{
			sb = new StringBuilder();
			OutputStream out = openFileSdCard(file);
			
			try
			{
				// write the headers
				writeHeaders(results, out);
				if (results.getCount() > 0 )
				{
					results.moveToFirst();
					do
					{
						writeRow(results, out);
					} while (results.moveToNext());
				}
			}
			catch (IOException ex)
			{
				// TODO throw an exception here that we can perhaps return to UI
			}
			finally
			{
				out.flush();
				out.close();
				sb = null;
			}
		}
	}
	
	private void writeRow(Cursor results, OutputStream out) throws IOException
	{
		// clear from previous use
		sb.setLength(0);
		String col;
		int count = results.getColumnCount();
		for (int i = 0; i < count; i++)
		{
			col = results.getString(i);
			if (col == null)
				sb.append(nullString);
			else
				sb.append(col);
			sb.append(delimiter);
		}
		sb.setLength(sb.length()-1);
		sb.append(newline);
		out.write(sb.toString().getBytes());
	}
	
	/**
	private FileOutputStream openFile(String file) throws FileNotFoundException
	{
		return context.openFileOutput(file, 0);
	}
	*/
	
	private OutputStream openFileSdCard(String filename) throws FileNotFoundException
	{
		File file = new File(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/" + filename);
		return new FileOutputStream(file);
	}
	
	private void writeHeaders(Cursor results, OutputStream out) throws IOException
	{
		// loop through all columns outputting names and delimiters
		for (String col : results.getColumnNames())
		{
			sb.append(col);
			sb.append(delimiter);
		}
		sb.setLength(sb.length() - 1);
		sb.append(newline);
		out.write(sb.toString().getBytes());
	}
}
