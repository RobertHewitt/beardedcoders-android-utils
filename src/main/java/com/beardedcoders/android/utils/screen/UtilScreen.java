package com.beardedcoders.android.utils.screen;

import android.app.Activity;
import android.content.Context;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Window;
import android.view.WindowManager.LayoutParams;

public class UtilScreen {
	public static float BRIGHTNESS_MAX_WINDOW = 100f;
	public static float BRIGHTNESS_MAX_SYSTEM = 255f;
	
	public static Window getWindow(Activity activity){
		return activity.getWindow();
	}
	
	public static LayoutParams getLayout(Activity activity){
		return activity.getWindow().getAttributes();
	}
	
	public static void setScreenFromParams(Activity activity, LayoutParams layout){
		Window t = UtilScreen.getWindow(activity);
		t.setAttributes(layout);
	}
	
	public static boolean isBrightnessValueAutoAdjust(Activity activity){
		return 
			Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC == 
			Integer.valueOf(Settings.System.getString(activity.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE));
	}
	
	public static void setBrightnessValueAutoAdjust(Activity activity , boolean autoAdjust){
		int adjustValue = autoAdjust 
					? Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC 
					: Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL;
		Settings.System.putString(activity.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, String.valueOf(adjustValue));
	}
	
	public static void setBrightnessSystem(Activity activity, double percentage) {
		int window = (int) (BRIGHTNESS_MAX_SYSTEM * percentage);
		window = window == 0 ? 1 : window;
		android.provider.Settings.System.putInt(activity.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, window);
	}
	
	public static double getBrightnessSystem(Activity activity) {
		double returnValue = 0f;
		try {
			int value = android.provider.Settings.System.getInt(activity.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
			returnValue = (float) value / BRIGHTNESS_MAX_SYSTEM;
		} catch (SettingNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return returnValue;
	}
	
	public static void setBrightnessWindow(Activity activity, double percentage) {
		percentage = percentage <= 0.01f ? 0.01f : percentage;
		LayoutParams layoutToEdit = UtilScreen.getLayout(activity);
		Window windowToEdit = UtilScreen.getWindow(activity);
		layoutToEdit.screenBrightness = (float) percentage;
		windowToEdit.setAttributes(layoutToEdit);
	}
	
	public static void setBrightnessWindowForceRefresh(Activity activity) {
		double percentage = UtilScreen.getBrightnessSystem(activity);
		LayoutParams layoutToEdit = UtilScreen.getLayout(activity);
		Window windowToEdit = UtilScreen.getWindow(activity);
		layoutToEdit.screenBrightness = (float) percentage;
		windowToEdit.setAttributes(layoutToEdit);
	}
	
	public static void setBothBrightnessSettings(Activity activity, double percentage){
		UtilScreen.setBrightnessSystem(activity, percentage);
		UtilScreen.setBrightnessWindow(activity, percentage);
	}
	
	public static int getPixelForDip(Context context, float dipValue) {
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, displayMetrics);
		return (int) px;
	}
	
}
