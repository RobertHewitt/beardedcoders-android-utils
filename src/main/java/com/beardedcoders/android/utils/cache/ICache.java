package com.beardedcoders.android.utils.cache;

public interface ICache<K, V> {

	public void add(K key, V value);
	public V get(K key);
	public void wipe();
}
