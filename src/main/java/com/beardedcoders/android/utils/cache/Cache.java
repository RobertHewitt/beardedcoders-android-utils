package com.beardedcoders.android.utils.cache;

import java.util.HashMap;
import java.util.Map;

/**
 * Class used for general purpose caching of objects.
 * 
 * Extra functionality surrounds - caching only a certain number of objects -
 * serialising data for a custom location
 *
 * todo - work in progress, this class is not functional yet.
 * 
 * @author Rob_2
 * 
 * @param <K>
 * @param <V>
 */
public abstract class Cache<K, V> implements ICache<K, V> {

	private static final long INFINITE = 0;
	private final Map<K, V> cache;
	/*
	 * cache limit of 0 means infinite
	 */
	protected final long cacheLimit;
	private long cacheSizeCurrent = 0;

	private final Object lockLimit = new Object();

	public Cache() {
		cache = initCacheStorage();
		cacheLimit = INFINITE;
	}

	protected Map<K, V> initCacheStorage() {
		return new HashMap<K, V>();
	}

	public void add(K key, V value) {
		synchronized (lockLimit) {
			// check if we need to care about space
			if (cacheLimit == INFINITE) 
			{
				cache.put(key, value);
			} else 
			{
				// make sure we have space
				if (cacheSizeCurrent < cacheLimit) {
					cacheSizeCurrent++;
					cache.put(key, value);
				} else {
					// we're in trouble, ran out of room in cache.
					// TODO -- ignore request or throw error?
				}
			}
		}
	}

	public V get(K key) {
		return cache.get(key);
	}

	public void wipe() {
		synchronized (lockLimit) {
			cache.clear();
			cacheSizeCurrent = 0;
		}
	}
}
