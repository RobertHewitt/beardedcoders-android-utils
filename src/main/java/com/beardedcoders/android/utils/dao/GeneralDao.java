package com.beardedcoders.android.utils.dao;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Abstract definition of a DAO for other classes to inherit from.  Provides the basic functionality to open and close a
 * database.
 * @author bilbovb
 *
 */
public abstract class GeneralDao
{
	protected final SQLiteOpenHelper dbHelper;
	protected final boolean readOnly;
	protected SQLiteDatabase database;	
	
	public GeneralDao(SQLiteOpenHelper dbHelper, boolean readOnly)
	{
		this.dbHelper = dbHelper;
		this.readOnly = readOnly;
		open(readOnly);
	}
	
	/**
	 * Close the database if open.
	 */
	public void close()
	{
		if (database!= null)
			
		{		
			database.close();
			database = null;
		}
	}
	
	/**
	 * Whether this DAO is used for read only access or not.
	 * @return true for read only access.
	 */
	public boolean getReadOnly()
	{
		return readOnly;
	}
	
	/**
	 * Open the database if it is not already open.
	 * @param readOnly whether to open in read only or write mode.
	 */
	public void open(boolean readOnly)
	{
		if (database == null)
			database = readOnly ? dbHelper.getReadableDatabase() : dbHelper.getWritableDatabase();
	}
	
	/**
	 * Return whether the database is open or closed.
	 * @return
	 */
	public boolean isOpen()
	{
		return (database == null) ? false : true;
	}
	
	
}
