package com.beardedcoders.android.utils.math;

/**
 * Created with IntelliJ IDEA.
 * User: Rob_2
 * Date: 07/05/12
 * Time: 17:42
 * Since: 1.1.0
 * To change this template use File | Settings | File Templates.
 */
public final class UtilMath
{
	public static int[] splitRangeByParts( final int range, int columns )
	{
		return splitRangeByParts(0, range, columns);
	}

	public static int[] splitRangeByParts( final int startRange, final int endRange, final int columns )
	{

		if ( columns <= 0 )
		{
			throw new IllegalArgumentException("param columns must be positive");
		}

		final int range = endRange - startRange;
		final int dividers = columns + 1;
		int dividersRemaining = dividers - 1; // this is -1 because we draw the first divider manually

		int[] iArr = new int[ dividers ];
		int currentVal = startRange;
		iArr[ 0 ] = currentVal;
		for ( int i = 1; i < ( dividers - 1 ); i++, dividersRemaining-- )
		{
			// work out how much space we are working with
			final int spaceRemaining = range - currentVal;
			int spacer = spaceRemaining / dividersRemaining;
			iArr[ i ] = currentVal + spacer;
			currentVal = iArr[ i ];
		}
		iArr[ dividers - 1 ] = endRange;
		return iArr;
	}
}
