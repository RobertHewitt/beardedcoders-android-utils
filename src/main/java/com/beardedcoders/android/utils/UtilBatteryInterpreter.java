package com.beardedcoders.android.utils;


import android.content.Intent;
import android.os.BatteryManager;

public class UtilBatteryInterpreter extends BatteryManager {
	public static final int FLAG_REPORT_HEALTH = 1;
	public static final int FLAG_REPORT_LEVEL = 2;
	public static final int FLAG_REPORT_POWER_SOURCE = 4;
	public static final int FLAG_REPORT_CURRENT_STATUS = 8;
	public static final int FLAG_REPORT_BATTERY_TECHNOLOGY = 16;
	public static final int FLAG_REPORT_TEMPERATURE = 32;
	public static final int FLAG_REPORT_VOLTAGE = 64;
	
	public static String getHealth(Intent batteryIntent){
		int health = batteryIntent.getIntExtra(EXTRA_HEALTH, 0);
		switch (health) 
		{
			case (BATTERY_HEALTH_DEAD): return "Dead";
			case (BATTERY_HEALTH_GOOD): return "Good";
			case (BATTERY_HEALTH_OVER_VOLTAGE): return "OverVoltage";
			case (BATTERY_HEALTH_OVERHEAT): return "HOT HOT HOT";
			case (BATTERY_HEALTH_UNSPECIFIED_FAILURE): return "Unspecified by API";
		}
		return "unknown";
	}
	
	public static String getPowerSource(Intent batteryIntent){
		int plugged = batteryIntent.getIntExtra(EXTRA_PLUGGED, 0);
		switch (plugged)
		{
			case (BATTERY_PLUGGED_AC): return "AC";
			case (BATTERY_PLUGGED_USB): return "USB";			
		}
		return "unknown";
	}
	
	public static String getStatus(Intent batteryIntent){
		int status = batteryIntent.getIntExtra(EXTRA_STATUS, 0);
		switch (status)
		{
			case (BATTERY_STATUS_CHARGING): return "Charging";
			case (BATTERY_STATUS_DISCHARGING): return "Discharging";
			case (BATTERY_STATUS_FULL): return "Full";
			case (BATTERY_STATUS_NOT_CHARGING): return "Not charging";
			case (BATTERY_STATUS_UNKNOWN): return "Unknown by API";
		}
		return "unknown";
	}
	
	public static int getPowerLevel(Intent batteryIntent) {
		return batteryIntent.getIntExtra(EXTRA_LEVEL, 0);
	}
	
	/**
	 * this will return a string representation of 
	 * @param batteryIntent
	 * @param flagsToReport
	 * @return
	 */
	public static String getBatteryData(Intent batteryIntent, int flagsToReport){
		String sReturn="";
		if (isFlagPresent(flagsToReport, FLAG_REPORT_HEALTH))
			sReturn = "Health : " + getHealth(batteryIntent) + "\n";
		if (isFlagPresent(flagsToReport, FLAG_REPORT_LEVEL))
			sReturn = sReturn + "Charge level: " + getPowerLevel(batteryIntent) + "\n";
		if (isFlagPresent(flagsToReport, FLAG_REPORT_POWER_SOURCE))
			sReturn = sReturn + "Power source: " + getPowerSource(batteryIntent) + "\n";
		if (isFlagPresent(flagsToReport, FLAG_REPORT_CURRENT_STATUS))
			sReturn = sReturn + "Current status: " + getStatus(batteryIntent) + "\n";
		if (isFlagPresent(flagsToReport, FLAG_REPORT_BATTERY_TECHNOLOGY))
			sReturn = sReturn + "Technology : " + getTechnology(batteryIntent) + "\n";
		if (isFlagPresent(flagsToReport, FLAG_REPORT_TEMPERATURE))
			sReturn = sReturn + "Temperature : " + getTemperature(batteryIntent) + "\n";
		if (isFlagPresent(flagsToReport, FLAG_REPORT_VOLTAGE))
			sReturn = sReturn + "Voltage : " + getVoltage(batteryIntent)+ "\n";
		return sReturn;
		// sReturn = sReturn + "EXTRA_SCALE: max battery level: " + batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, 0) + "\n";
		// sReturn = sReturn + "EXTRA_PRESENT: is battery in: " + batteryIntent.getIntExtra(BatteryManager.EXTRA_PRESENT, 0) + "\n";
		// sReturn = sReturn + "EXTRA_ICON_SMALL: small stat res id: " + batteryIntent.getIntExtra(BatteryManager.EXTRA_ICON_SMALL, 0) + "\n";
	}
	
	public static int getVoltage(Intent batteryIntent) {
		return batteryIntent.getIntExtra(EXTRA_VOLTAGE, 0);
	}

	public static String getTechnology(Intent batteryIntent) {
		return batteryIntent.getStringExtra(EXTRA_TECHNOLOGY);
	}

	public static float getTemperature(Intent batteryIntent) {
		float data = batteryIntent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0);
		return data /10 ;
	}
	
	public static boolean getPhoneCharging(Intent batteryIntent) {
		int batteryCharging = batteryIntent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0);
		switch (batteryCharging) {
			case BATTERY_PLUGGED_AC:
			case BATTERY_PLUGGED_USB:
				return true;
			default: 
				return false;
		}
	}

	public static boolean isFlagPresent(int bitFlag, int flagToCheck) {
//		AppSettings.log(UtilBatteryInterpreter.class, "isFlagPresent", 
//								new StringBuffer()
//							.append("bitflagValue: " + bitFlag)
//							.append(" value to find: " + flagToCheck)
//							.append(" result: " + ((bitFlag & flagToCheck) != 0)) 
//							.append("\n")
//							.toString());
		return (bitFlag & flagToCheck) != 0;
	}
}
