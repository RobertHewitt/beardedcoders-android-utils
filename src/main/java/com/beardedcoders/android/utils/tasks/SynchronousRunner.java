package com.beardedcoders.android.utils.tasks;

/**
 * User: Rob_2
 * Date: 04/04/13
 * Time: 22:38
 *
 * Performs the task on the calling thread
 */
public class SynchronousRunner<Params, Progress, Result> implements TaskRunner<Params, Progress, Result> {

    final private Task<Params,Progress,Result> task;

    public SynchronousRunner(Task<Params, Progress, Result> task) {
        this.task = task;
    }

    @Override
    public void run(Params input) {
        task.onPreExecute();
        final Result result = task.doInBackground(this, input);
        task.onPostExecute(result);
    }

    @Override
    public void publishProgress(Progress... progresses) {

    }
}
