package com.beardedcoders.android.utils.tasks;

/**
 * User: Rob_2
 * Date: 04/04/13
 * Time: 22:32
 * <p/>
 * Allowing any runner (async or otherwise) to perform the execution of the task
 */
public interface TaskRunner<Params, Progress, Result> {

    void run(Params input);

    void publishProgress(Progress... progresses);
}