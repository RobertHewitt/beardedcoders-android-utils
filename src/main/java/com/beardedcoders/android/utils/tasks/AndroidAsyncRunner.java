package com.beardedcoders.android.utils.tasks;

import android.os.AsyncTask;

/**
 * User: Rob_2
 * Date: 04/04/13
 * Time: 22:38
 *
 * Full wrapper for an android async task.
 */
public class AndroidAsyncRunner<Params, Progress, Result> implements TaskRunner<Params, Progress, Result> {

    final private Task<Params,Progress,Result> task;
    private AsyncTask<Params,Progress,Result> asyncTask;

    public AndroidAsyncRunner(Task<Params, Progress, Result> task) {
        this.task = task;
    }

    @Override
    public void publishProgress(Progress... progresses) {
    }

    @Override
    public void run(Params input) {
        asyncTask = new AsyncTask<Params, Progress, Result>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                task.onPreExecute();
            }

            @Override
            protected void onPostExecute(Result result) {
                super.onPostExecute(result);
                task.onPostExecute(result);
            }

            @Override
            protected void onProgressUpdate(Progress... values) {
                super.onProgressUpdate(values);
                task.onProgressUpdate(values);
            }

            @Override
            protected void onCancelled(Result result) {
                super.onCancelled(result);
                task.onCancelled(result);
            }

            @Override
            protected void onCancelled() {
                super.onCancelled();
                task.onCancelled();
            }

            @Override
            protected Result doInBackground(Params... paramses) {
                return task.doInBackground(AndroidAsyncRunner.this, paramses);
            }

        };
        asyncTask.execute(input);
    }
}
