package com.beardedcoders.android.utils.tasks;

/**
 * User: Rob_2
 * Date: 04/04/13
 * Time: 22:23
 * <p/>
 * A wrapper for an async task in android
 * this allows for creation of tests vs a non android dependency
 */
public abstract class Task<Params, Progress, Result> {

    protected void onPreExecute() {
    }

    protected void onPostExecute(Result result) {
    }

    protected void onProgressUpdate(Progress... values) {
    }

    protected void onCancelled(Result result) {
    }

    protected void onCancelled() {
    }

    protected void publishUpdate(Progress... progresses) {

    }

    abstract protected Result doInBackground(TaskRunner runner, Params... inputs);

}
